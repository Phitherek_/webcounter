# frozen_string_literal: true

require_relative './config/environment'

loop do
  counter = Counters::CounterFetcher.new.call
  Counters::CounterUpdater.new(value: counter&.value.to_i + 1).call
  sleep 10
end
