# frozen_string_literal: true

class CounterUpdateChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'counter_update'
  end
end
