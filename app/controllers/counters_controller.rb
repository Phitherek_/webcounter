# frozen_string_literal: true

class CountersController < ApplicationController
  def show
    @str = Counters::CounterStringBuilder.new.call
  end

  def reset
    Counters::CounterResetter.new.call
    redirect_to counter_url
  end
end
