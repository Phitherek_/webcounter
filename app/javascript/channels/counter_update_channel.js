import consumer from "./consumer";

consumer.subscriptions.create({ channel: 'CounterUpdateChannel' }, {
  received(data) {
    this.appendValue(data);
  },

  appendValue(data) {
    let element = document.querySelector("#strContainer");
    let str = element.innerHTML;
    str = str + ' ' + data.value;
    element.innerHTML = str;
  }
})