# frozen_string_literal: true

module Counters
  class CounterResetter
    def call
      CounterUpdater.new(value: 1).call
    end
  end
end
