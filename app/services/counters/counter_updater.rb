# frozen_string_literal: true

module Counters
  class CounterUpdater
    attr_reader :value

    def initialize(value:)
      @value = value
    end

    def call
      counter = CounterFetcher.new.call || Counter.new
      counter.update(value: value)
      ActionCable.server.broadcast('counter_update', { value: value })
    end
  end
end
