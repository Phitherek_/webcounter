# frozen_string_literal: true

module Counters
  class CounterFetcher
    def call
      Counter.first
    end
  end
end
