# frozen_string_literal: true

module Counters
  class CounterStringBuilder
    def call
      counter = CounterFetcher.new.call
      str = ''
      return str if counter.blank?

      1.upto(counter.value) do |i|
        str += if i == counter.value
                 i.to_s
               else
                 "#{i} "
               end
      end
      str
    end
  end
end
