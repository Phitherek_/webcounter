# Webcounter

A sample app for DevOps exercises.

## Dependencies

* Ruby 3.0.1
* Preferred Ruby environment manager: RVM
* PostgreSQL
* Redis

## Note

For all commands below without `bundle` use `bundle exec` to run command in context of the bundle

## Setup (with dev and testing gems)

```
bundle install
(create PostgreSQL user with optional CREATEDB permission according to config/database.yml)
rake db:create (or create database as PostgreSQL admin and assign it to user)
rake db:schema:load
```

## Production setup
```
bundle config --local deployment true
bundle config --local without development:test
bundle install
(create PostgreSQL user with optional CREATEDB permission according to config/database.yml)
rake db:create (or create database as PostgreSQL admin and assign it to user)
rake db:schema:load (on first deployment only)
rake db:migrate (on subsequent deployments)
rake assets:precompile
rake assets:clean
```

## Running server
```
rails server
```

## Running counter worker
```
ruby counter_worker_control.rb (start|stop|run) (start - run in background, run - run in foreground)
```

## Expected result

A page with a button to reset counter and live updated number string based on counter value.

## Running tests

```
rake spec
```
or
```
rspec spec
```

## Running linter

```
rubocop
```

## Contributing

* Fork repo (if you do not have push privileges)
* Create branch
* Make changes
* Run tests and linter
* Make MR

Have fun!