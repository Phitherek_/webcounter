# frozen_string_literal: true

class CreateCounters < ActiveRecord::Migration[6.1]
  def change
    create_table :counters do |t|
      t.bigint :value, null: false, default: 1
      t.timestamps
    end
  end
end
