# frozen_string_literal: true

Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resource :counter, only: [:show] do
    member do
      post :reset
    end
  end
  root to: 'counters#show'
end
