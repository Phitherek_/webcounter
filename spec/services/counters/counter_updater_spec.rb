# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Counters::CounterUpdater do
  describe '#call' do
    let(:actioncable_server) { instance_double(ActionCable::Server::Base) }

    before do
      expect(ActionCable).to receive(:server).and_return(actioncable_server)
    end

    context 'when counter does not exist' do
      it 'creates new counter with given value' do
        expect(actioncable_server).to receive(:broadcast).with('counter_update', { value: 2 })
        described_class.new(value: 2).call
        expect(Counter.first).to be_present
        expect(Counter.first.value).to eq(2)
      end
    end

    context 'when counter does exist' do
      let!(:counter) { Counter.create(value: 1) }

      it 'updates existing counter with given value' do
        expect(actioncable_server).to receive(:broadcast).with('counter_update', { value: 2 })
        described_class.new(value: 2).call
        expect(counter.reload).to be_truthy
        expect(counter.value).to eq(2)
      end
    end
  end
end
