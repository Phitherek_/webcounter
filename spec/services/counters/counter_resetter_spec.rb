# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Counters::CounterResetter do
  let(:updater_instance) { instance_double(Counters::CounterUpdater) }

  describe '#call' do
    it 'calls CounterUpdater setting value to 1' do
      expect(Counters::CounterUpdater).to receive(:new).with(value: 1).and_return(updater_instance)
      expect(updater_instance).to receive(:call)

      described_class.new.call
    end
  end
end
