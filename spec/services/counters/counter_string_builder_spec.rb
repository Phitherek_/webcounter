# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Counters::CounterStringBuilder do
  let(:fetcher_instance) { instance_double(Counters::CounterFetcher) }

  before do
    expect(Counters::CounterFetcher).to receive(:new).and_return(fetcher_instance)
  end

  describe '#call' do
    context 'without counter' do
      it 'returns empty string' do
        expect(fetcher_instance).to receive(:call).and_return(nil)
        expect(described_class.new.call).to eq('')
      end
    end

    context 'with counter' do
      let!(:counter) { Counter.create(value: 10) }

      it 'returns proper string' do
        expect(fetcher_instance).to receive(:call).and_return(counter)
        expect(described_class.new.call).to eq('1 2 3 4 5 6 7 8 9 10')
      end
    end
  end
end
