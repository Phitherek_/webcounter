# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Counters::CounterFetcher do
  let!(:counter) { Counter.create(value: 1) }
  let!(:counter2) { Counter.create(value: 1) }

  describe '#call' do
    it 'returns first counter' do
      expect(described_class.new.call).to eq(counter)
    end
  end
end
