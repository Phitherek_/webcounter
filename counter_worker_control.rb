# frozen_string_literal: true

require 'daemons'

Daemons.run('counter_worker.rb')
